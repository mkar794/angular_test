import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { Observable, fromEventPattern } from 'rxjs';
import { data } from 'src/app/model/data.model'

const headerOption = {
  headers: new HttpHeaders({
    'content-Type': 'application/json'
  })
}

@Injectable()
export class ImageService {

 url = "http://localhost:3000/data/";

  constructor(
    private http: HttpClient
  ) { }

  getAllImages(): Observable<data[]> {
    return this.http.get<data[]>(this.url, headerOption )
  }
}
