import { Component, OnInit } from '@angular/core';
import { ImageService } from 'src/app/modules/shared/image.service';
import { data } from 'src/app/model/data.model';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-big-images',
  templateUrl: './big-images.component.html',
  styleUrls: ['./big-images.component.css']
})
export class BigImagesComponent implements OnInit {

  data: data[];


  constructor(
    private dataImage: ImageService,
    config: NgbCarouselConfig
  ) {
    config.interval = 2000;
    config.wrap = true;
    config.keyboard = false;
    config.pauseOnHover = false;
  }

  ngOnInit() {
    this.getAllImage();
  }

  getAllImage() {
    this.dataImage.getAllImages().subscribe(
      (data:data[]) => {
        this.data = data;
      }
    )
  }

}
