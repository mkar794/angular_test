import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmallImagesComponent } from './small-images/small-images.component';
import { BigImagesComponent } from './big-images/big-images.component';
import { ImageService } from '../shared/image.service';


@NgModule({
  declarations: [SmallImagesComponent, BigImagesComponent],
  imports: [
    CommonModule
  ],
  exports: [SmallImagesComponent, BigImagesComponent],
  providers:[ImageService]
})
export class ImagesModule { }
