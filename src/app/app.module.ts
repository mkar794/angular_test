import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
// import { TopSliderComponent } from './top-slider/top-slider.component';
// import { RightSliderComponent } from './right-slider/right-slider.component';
// import { HomeComponent } from './home/home.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ImagesModule } from './modules/images/images.module';
import { HttpClientModule } from '@angular/common/http';
import { SliderModule } from 'angular-image-slider';


@NgModule({
  declarations: [
    AppComponent,
  //  TopSliderComponent,
  //  RightSliderComponent,
  //  HomeComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    NgbModule,
    ImagesModule,
    HttpClientModule,
    SliderModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
